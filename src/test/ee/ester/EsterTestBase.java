package ee.ester;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selectors.withText;
import static com.codeborne.selenide.Selenide.*;

/**
 * Created by jevgenisa on 21.11.2015.
 */
public abstract class EsterTestBase {


    public static final String LOGIN_USR = "";
    public static final String LOGIN_PWD = "";
    public static final String LOGIN_LIB = "";
    public static final String LOGIN_FULLNAME = "";


    @Before
    public void startTest() {
        open("http://ester.ee/");
        $("#keel_eng").click();
    }

    @After
    public void endTest(){
        close();
    }

    protected void switchWindow(WebDriver driver, String parentWindowHandler, String elementName, String elementText ) {

        String subWindowHandler = null;

        Set<String> handles = driver.getWindowHandles(); // get all window handles
        Iterator<String> iterator = handles.iterator();
        while (iterator.hasNext()){
            subWindowHandler = iterator.next();
            if (subWindowHandler.equals(parentWindowHandler))
                continue;
            driver.switchTo().window(subWindowHandler);
            ElementsCollection titles = $$(elementName);///.$(withText("Please enter new information and press Submit"));
            WebElement title = titles.find(text(elementText));
            if (title!=null){
                break;
            }
        }
    }

    protected void login(){
        $("#log_in").click();
        $(withText(LOGIN_LIB)).click();
        $("#my_code").setValue(LOGIN_USR);
        $("#pin").setValue(LOGIN_PWD);
        $("#edasta").click();

    }

    protected WebElement getUserDetailsSubmenu(){
        return $(getMainContent()).$(".site-results-filter");
    }

    protected WebElement getItemDetailsSubmenu(){
        return $(getMainContent()).$(".tabs-header").$(".nav-tabs");
    }

    protected WebElement getMainContent(){
        return $(".site[role='main']");
    }

    protected void clickAvailableItem() {
        $$(".results-list tbody tr").get(4).$("td").$$("div").get(1).$$("div").get(1).$("h2").$("a").click();
    }

    protected void checkAvailability() {
        $("#links_table").shouldHave(text("Status"));
    }

    protected void clickItem() {
        Collection<SelenideElement> elements = $$(".browseList tbody tr");
        for(SelenideElement elem : elements){
            if(elem.has(cssClass("browseEntry"))){
                SelenideElement numberOfEntries = elem.$(".browseEntryEntries");
                int n = Integer.parseInt(numberOfEntries.getText());
                if(n == 1){
                    SelenideElement entryData = elem.$(".browseEntryData");
                    $(entryData).$$("a").get(1).click();
                    return;
                }
            }
        }
    }

    protected void search(String library, String keyword, String searchWord){
        if(library!="")selectLibrary(library);
        if(keyword!="")selectKeyword(keyword);
        enterSearchword(searchWord);
        submit();
        checkSearchResult(searchWord);
    }


    protected void selectLibrary(String libName){
        $("#skoop").click();
        $(withText(libName)).click();
    }

    protected void selectKeyword(String keyword) {
        $("a[href*='#dropdown-select-filter']").click();
        $(withText(keyword)).click();

    }

    protected void enterSearchword(String searchword) {
        $("#otsi").setValue(searchword);
    }

    protected void submit() {
        $("#otsi_button").click();
    }

    protected void checkSearchResult(String searchWord) {
        Collection<SelenideElement> elements = $$(".browseList tbody tr");
        for(SelenideElement elem : elements){
            if(elem.has(cssClass("browseEntry"))){
                elem.shouldHave(text(searchWord));
            }
        }
    }

    protected void rateItem(int stars){
        WebElement elem=$("span.rateData").$("#rateneed1").$$("a").get(stars-1);
        ((JavascriptExecutor) WebDriverRunner.getWebDriver()).executeScript("arguments[0].click();", elem);
    }

}
