package ee.ester;


import com.codeborne.selenide.WebDriverRunner;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import static com.codeborne.selenide.CollectionCondition.texts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.withText;
import static com.codeborne.selenide.Selenide.*;

public class EsterTest extends EsterTestBase{


	@Test
	public void should_search_FR1() {
		search("TU Institute of Estonia Demography", "Title", "functional");
		checkSearchResult("functional");
	}

	@Test
	public void should_show_item_details_FR2() {
		search("TU Institute of Estonia Demography", "Title", "functional");
		clickItem();
		$(".result-page-content").shouldHave(text("Author"));
		$(".result-page-content").shouldHave(text("Title"));
		$(".result-page-content").shouldHave(text("Imprint"));
		$(".result-page-content").shouldHave(text("Place of publ."));
		$(".result-page-content").shouldHave(text("Description"));
		$(".result-page-content").shouldHave(text("Permalink"));
	}

	@Test
	public void should_show_availability_of_item_FR3(){
		search("", "", "functional");
		clickAvailableItem();
		checkAvailability();
	}

	@Test
	public void should_show_new_arrivals_FR4(){
		$("#uudiskirjandus").click();
		$(".site").shouldHave(text("New Arrivals"));
	}

	@Test
	public void should_show_publishing_soon_FR5(){
		$("#ilmumas").click();
		$(".results-list").shouldBe(visible);
	}

	@Test
	public void should_login_successfully_FR6(){
		login();
		$("#name").shouldHave(text(LOGIN_FULLNAME));
		$(getUserDetailsSubmenu()).$(withText("Personal Information")).shouldHave(cssClass("active"));
	}

	@Test
	public void should_modify_personal_data_FR7(){
		login();
		$(getMainContent()).$(withText("Modify Your Personal Information")).click();

		WebDriver driver = WebDriverRunner.getWebDriver();
		String parentWindowHandler = driver.getWindowHandle();
		switchWindow(driver, parentWindowHandler, "legend", "Please enter new information and press Submit");

		String newNumber="12345678";
		$("#tele2").setValue(newNumber);
		$("#edasta").click();
		driver.switchTo().window(parentWindowHandler);
		refresh();
		$("#telefon2").shouldHave(text(newNumber));
		//$(getUserDetailsSubmenu()).$(withText("Personal Information"))
	}

	@Test
	public void should_modify_personal_data_FR8(){
		login();
		$(getMainContent()).$(withText("Modify your Password")).click();

		WebDriver driver = WebDriverRunner.getWebDriver();
		String parentWindowHandler = driver.getWindowHandle();
		switchWindow(driver, parentWindowHandler, "h1", "Modify your Password");
		$(getMainContent()).$("form").has(text("Current password"));
		$(getMainContent()).$("form").has(text("New password"));
	}

	@Test
	@Ignore("It is not possible to test it with the live data")
	public void should_display_fines_FR9(){
		login();
		$(getMainContent()).$(withText("Modify your Password")).click();

		WebDriver driver = WebDriverRunner.getWebDriver();
		String parentWindowHandler = driver.getWindowHandle();
		switchWindow(driver, parentWindowHandler, "h1", "Modify your Password");
		$(getMainContent()).$("form").has(text("Current password"));
		$(getMainContent()).$("form").has(text("New password"));
	}

	@Test
	@Ignore("It is not possible to test it with the live data")
	public void should_extend_checkout_FR10(){
	}

	@Test
	public void should_rate_item_FR11(){
		login();
		selectLibrary(LOGIN_LIB);
		enterSearchword("tepandi");
		$("button.btn-submit-search").click();
		clickAvailableItem();
		String itemName = $$("td.bibInfoLabel").find(text("Title")).parent().$$("td").get(1).getText();
		$(getItemDetailsSubmenu()).$(withText("Additional Information")).click();
		//rateItem(5);
		$("#login_name").click();
		$("#my-settings-box").should(appear);
		$("#my-settings-box").$(withText("Ratings")).click();
		$("form[name='PATRATINGSFORM']").shouldHave(text(itemName));

	}

	@Test
	public void should_review_item_FR12(){
		login();
		selectLibrary(LOGIN_LIB);
		enterSearchword("tepandi");
		$("button.btn-submit-search").click();
		clickAvailableItem();
		$(getItemDetailsSubmenu()).$(withText("Additional Information")).click();
		$$("a").find(text("Add a Review")).click();
		WebDriver driver = WebDriverRunner.getWebDriver();
		String parentWindowHandler = driver.getWindowHandle();
		switchWindow(driver, parentWindowHandler, "h1", "Add a Review");
		$("#headline").shouldBe(visible);
		$("textarea[name='body']").shouldBe(visible);

	}

	@Test
	public void should_change_language_FR13(){
		$("#keel_eng").has(cssClass("active"));
		$("#keel_est").shouldNotHave(cssClass("active"));
		$("#keel_est").click();
		$("#keel_est").has(cssClass("active"));
		$("#keel_eng").shouldNotHave(cssClass("active"));
		$$(".nav li").contains(texts("Uudiskirjandus", "Ainepaketid", "Ilmumas", "Abi"));
	}

	@Test
	public void should_create_and_populate_item_list_FR14(){
		login();
		selectLibrary(LOGIN_LIB);
		enterSearchword("tepandi");
		$("button.btn-submit-search").click();
		clickAvailableItem();
		String listName = "test for SQ 1234";
		$("#save_records").click();
		$("select[name='listname']").selectOption("- Create a new list -");
		$("#newlistname").setValue(listName);
		$("form[name='newlistForm']").submit();

		$("#login_name").click();
		$("#my-settings-box").should(appear);
		$("#my-settings-box").$(withText("Lists")).click();

		$("form[name='mylists_form']").shouldHave(text(listName));
	}

	@Test
	public void should_save_search_rule_FR15(){
		login();
		selectLibrary(LOGIN_LIB);
		String searchExpression = "tepandi";
		enterSearchword(searchExpression);
		$("button.btn-submit-search").click();
		$("li.save_srch").$(withText("Save search")).click();

		$("#login_name").click();
		$("#my-settings-box").should(appear);
		$("#my-settings-box").$(withText("Preferred Searches")).click();

		$("form[name='PSEARCHFORM']").shouldHave(text(searchExpression));
	}
	



}
